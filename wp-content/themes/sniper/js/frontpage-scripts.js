jQuery(document).ready(function ($){

	// Initialize Tabslet
	$('.tabs').tabslet({
		container: '#tabs-container',
		active: 1,
		animation: true
	});

	// Initialize Tooltipster
	$('.tooltip').tooltipster({
		theme: 'tooltipster-light'
	});

});
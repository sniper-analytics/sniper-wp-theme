<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Sniper Analytics</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
        
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <div id="wrapper">
        	<header id="site-header" class="full-width">
                <div class="container">
        			<div id="logo">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sniper-analytics-logo-small.png" width="85" height="50" alt="Sniper Analytics small logo" /> <span>Matchmaking</span>
                    </div>
                    <nav id="top-nav">
                        <ul>
                            <li><a href="#benefits">Benefits</a></li>
                            <li><a href="#pricing">Pricing</a></li>
                            <li><a href="#features">Features</a></li>
                            <li class="last"><a href="#see-how-it-works">See how it works</a></li>
                        </ul>
                    </nav>
                </div>
            </header>
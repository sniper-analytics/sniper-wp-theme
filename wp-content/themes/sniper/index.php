<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<article>
			<?php if ( has_post_thumbnail() ) {
				the_post_thumbnail();
			} ?>
			<div class="container">
				<header class="content-header">
					<h1><?php the_title(); ?></h1>
				</header>

				<div class="content-page">
					<?php the_content(); ?>
				</div>
			</div>
		</article>

	<?php endwhile; else : ?>
		<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>

<?php get_footer(); ?>
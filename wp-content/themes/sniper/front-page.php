<?php get_header(); ?>

<div id="intro" class="full-width">
	<div class="container">
			<div id="featured-cta">
				<div class="container">
					<div class="top">
						<h1>Agent-to-Lead matching for Call Centers</h1>
						<p>Increase telemarketing revenue by up to +20%</p>
					</div>
					<div class="button-container">
						<a class="button" href="/try-matchmaking/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/arrow-right-fast.png" width="81" height="46" alt="Arrow Right" /> Try for 20 000 leads free-of-charge</a>
					</div>
				</div>
			</div>
	</div>
</div>

<div id="benefits" class="full-width dark-grey">
	<div class="container">
		<div class="title">
			<h2>You can't change your leads</h2>
			<p class="subtitle">But you can change who calls to whom</p>
		</div>
		<div class="row grid-container">
			<div class="icon grid-10">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/euro-icon.png" width="64" height="64" alt="Euro icon" />
			</div>
			<div class="text grid-90">
				<h3>Increase <span class="sniper-red">sales</span></h3>
				<p>Each agent has a unique sales profile. One performs well when calling to young adults, another to a certain city and third to 70-year old men from rural areas.</p>
				<p>Before Matchmaking, it was practically impossible to figure out these small differences and use them to improve sales.</p>
			</div>
		</div>
		<div class="row grid-container">
			<div class="icon grid-10">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/agent-icon.png" width="64" height="60" alt="Euro icon" />
			</div>
			<div class="text grid-90">
				<h3>Decrease <span class="sniper-red">agent turnover</span></h3>
				<p>New agents are bound to quit if they struggle to make sales early on. In provision-based job it's totally understandable, as they have to pay their bills too.</p>
				<p>Matchmaking makes sure that each agent get leads that fit their profile. This way they have the best chances to succeed and keep working for your company.</p>
			</div>
		</div>
		<div class="row grid-container">
			<div class="icon grid-10">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/euro-icon.png" width="64" height="64" alt="Euro icon" />
			</div>
			<div class="text grid-90">
				<h3>Save <span class="sniper-red">time</span></h3>
				<p>When your agent calls to a person who does not fit her profile, she has odds stacked against her even before the line opens.</p>
				<p>Her voice, choice of words or personality might annoy the person she is talking to and that's when the call is usually lost before the actual conversation begins.</p>
				<p>Matchmaking avoids clear mismatches to save valuable time for real sales opportunities.</p>
			</div>
		</div>
	</div>
</div>

<div id="pricing" class="full-width light-grey">
	<div class="container">
		<div class="title">
			<h2>Plans & Pricing</h2>
			<p class="subtitle">Try Matchmaking free-of-charge for 20 000 leads</p>
		</div>
		<div class="row">
			<table id="pricing-table">
				<thead>
					<tr>
						<th class="empty"></th>
						<th>Trial</th>
						<th>Fixed</th>
						<th>Pay-as-you-Go</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>Leads</th>
						<td>20 000<span class="sub small">maximum</span></td>
						<td>20 000<span class="sub small">minimum</span></td>
						<td>Unlimited</td>
					</tr>
					<tr>
						<th>Agents</th>
						<td>Unlimited</td>
						<td>Unlimited</td>
						<td>Unlimited</td>
					</tr>
					<tr>
						<th>Reporting</th>
						<td>Weekly</td>
						<td>Bi-weekly</td>
						<td>Bi-weekly</td>
					</tr>
					<tr>
						<th>Price</th>
						<td class="price">0.00€<span class="sub small">per lead</span></td>
						<td class="price">0.07€<span class="sub small">per lead</span></td>
						<td class="price">0.10€<span class="sub small">per lead</span></td>
					</tr>
					<tr class="guarantee">
						<th>Guarantee</th>
						<td><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/cross.png" width="32" height="32" /></td>
						<td><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/cross.png" width="32" height="32" /></td>
						<td><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/checkmark.png" width="32" height="32" class="tooltip" title="You only pay for the Matchmaking Events where Matched group performs better than randomly chosen Control group" /></td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td class="empty"></td>
						<td><button>Start Trial</button></td>
						<td><button>Choose Plan</button></td>
						<td><button>Choose Plan</button></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>

<div id="features" class="full-width dark-grey">
	<div class="container">
		<div class="title">
			<h2>Features</h2>
		</div>
		<div class="row">
			<div class="tabs">
				<ul class="horizontal grid-container">
					<li class="grid-33"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/machine-learning.png" width="112" height="128" alt="Machine-learning" /><a href="#tab-1">Machine-learning</a></li>
					<li class="grid-33"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/one-click-matching.png" width="100" height="128" alt="One-click matching" /><a href="#tab-2">One-click matching</a></li>
					<li class="grid-33"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/detailed-reports.png" width="128" height="128" alt="Detailed reports" /><a href="#tab-3">Detailed reports</a></li>
				</ul>
				<div id="tabs-container">
					<div id="tab-1">
						<p>Testing tab 1</p>
					</div>
					<div id="tab-2">
						<p>Testing tab 2</p>
					</div>
					<div id="tab-3">
						<p>Testing tab 3</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="see-how-it-works" class="full-width sniper-red">
	<div class="container">
		<div class="title">
			<h2>See how it works</h2>
		</div>	
	</div>
</div>

<div id="available-for" class="full-width grey">
	<div class="container">
		<div class="title">
			<h2>Available for</h2>
		</div>
	</div>
</div>

<?php get_footer(); ?>
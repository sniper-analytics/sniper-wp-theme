<?php

add_theme_support( 'post-thumbnails' );

### Register and Enqueue Scripts

add_action( 'wp_enqueue_scripts', 'enqueue_and_register_scripts_and_styles' );

function enqueue_and_register_scripts_and_styles() {

	// Register Scripts: wp_register_script( $handle, $src, $deps, $ver, $in_footer );
    wp_register_script( 'modernizr', get_stylesheet_directory_uri().'/js/vendor/modernizr-latest.js' );
    wp_register_script( 'smooth-scroll', get_stylesheet_directory_uri().'/js/vendor/jquery.smooth-scroll.js', array('jquery'), '1.5.5', true );
    wp_register_script( 'tabslet', get_stylesheet_directory_uri().'/js/vendor/jquery.tabslet.min.js', array('jquery'), '1.7.0', true );
    wp_register_script( 'tooltipster', get_stylesheet_directory_uri().'/js/vendor/jquery.tooltipster.min.js', array('jquery'), '3.0', true);
    wp_register_script( 'global-scripts', get_stylesheet_directory_uri().'/js/global-scripts.js', array('jquery', 'smooth-scroll'), 'latest', true );
    wp_register_script( 'frontpage-scripts', get_stylesheet_directory_uri().'/js/frontpage-scripts.js', array('jquery', 'tabslet', 'tooltipster'), 'latest', true);

    // Enqueue Scripts: wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
    wp_enqueue_script( 'modernizr' );
    wp_enqueue_script( 'global-scripts' );

    // Enqueue scripts on specific pages only

    if ( is_front_page() ) {
        wp_enqueue_script( 'frontpage-scripts' );
    }

    // Register Styles: wp_register_style( $handle, $src, $deps, $ver, $media );
    wp_register_style( 'google-fonts', '//fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Open+Sans:400italic,400,300,300italic,600,700' );
    wp_register_style( 'reset', get_stylesheet_directory_uri().'/css/html5-doctor-reset-stylesheet.css' );
    wp_register_style( 'base', get_stylesheet_directory_uri().'/css/base.css');
    wp_register_style( 'main', get_stylesheet_directory_uri().'/style.css', array('google-fonts', 'reset', 'base') );
    wp_register_style( 'media-queries', get_stylesheet_directory_uri().'/css/media-queries.css', array('main') );
    wp_register_style( 'unsemantic-grid', get_stylesheet_directory_uri().'/css/vendor/unsemantic-grid-responsive-tablet-no-ie7.css');
    wp_register_style( 'tooltipster-css', get_stylesheet_directory_uri().'/css/vendor/tooltipster.css');
    wp_register_style( 'tooltipster-theme-light', get_stylesheet_directory_uri().'/css/vendor/tooltipster-light.css', array('tooltipster-css'));

    // Enqueue Styles: wp_enqueue_style( $handle, $src, $deps, $ver, $media );
    wp_enqueue_style( 'main' );
    wp_enqueue_style( 'media-queries' );
    wp_enqueue_style( 'unsemantic-grid' );
    wp_enqueue_style( 'tooltipster-theme-light' );

    // Enqueue styles on specific pages only

}